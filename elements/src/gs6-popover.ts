import { LitElement, html, customElement, css } from "lit-element";

@customElement("gs6-popover")
export class Popover extends LitElement {
  static styles = css`
    :host {
      display: block;
    }
  `;

  render() {
    return html`<slot></slot>`;
  }

  onClick = (event: Event) => {
    if (!(event as any).__gs6_ignore_dismissal) {
      this.dispatchEvent(
        new CustomEvent("dismiss", {
          bubbles: true,
          cancelable: true,
          composed: true, // makes the event jump shadow DOM boundary
        })
      );
    }
  };

  onKeyDown = (event: KeyboardEvent) => {
    if (event.code === "Escape") {
      this.dispatchEvent(
        new CustomEvent("dismiss", {
          bubbles: true,
          cancelable: true,
          composed: true, // makes the event jump shadow DOM boundary
        })
      );
    }
  };

  connectedCallback() {
    super.connectedCallback();

    this.addEventListener("click", (event: Event) => {
      (event as any).__gs6_ignore_dismissal = true;
    });

    setTimeout(() => {
      document.body.addEventListener("click", this.onClick);
      document.body.addEventListener("keydown", this.onKeyDown);
    });
  }

  disconnectedCallback() {
    super.disconnectedCallback();

    document.body.removeEventListener("click", this.onClick);
    document.body.removeEventListener("keydown", this.onKeyDown);
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "gs6-popover": Popover;
  }
}
