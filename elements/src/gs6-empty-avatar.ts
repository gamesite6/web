import { LitElement, html, customElement, css } from "lit-element";

@customElement("gs6-empty-avatar")
export class EmptyAvatar extends LitElement {
  static styles = css`
    :host {
      display: block;
      height: 1em;
      width: 1em;
      box-sizing: border-box;
      border-radius: 0.125em;
      border-style: dashed;
      border-color: #0006;
      border-width: 0.1em;
    }
    :host(.inline) {
      display: inline-block;
      transform: scale(1.3) translateY(0.15em);
      margin-left: 0.25em;
      margin-right: 0.25em;
    }
  `;

  render() {
    return html``;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "gs6-empty-avatar": EmptyAvatar;
  }
}
