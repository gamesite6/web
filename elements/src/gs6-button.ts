import { LitElement, html, customElement, css } from "lit-element";

@customElement("gs6-button")
export class Button extends LitElement {
  static styles = css`
    :host {
      display: block;
      height: 2.5em;

      --shadow-height: 0.25rem;
    }

    button {
      height: calc(100% - var(--shadow-height));

      border-style: solid;
      border-color: rgba(0, 0, 0, 0.2);
      border-width: 0.0625rem;
      border-radius: 0.3rem;
      cursor: pointer;
      padding: 0 1.5em;
      margin-bottom: var(--shadow-height);
      font-size: inherit;
      font-family: inherit;

      display: grid;
      place-content: center;
    }
    button:not(:disabled) {
      cursor: pointer;
    }

    button {
      background-color: white;
      color: inherit;
      filter: drop-shadow(0 var(--shadow-height) 0 rgb(220, 220, 220));
    }
    button:active:not(:disabled) {
      filter: drop-shadow(
        0 calc(var(--shadow-height) / 2) 0 rgb(220, 220, 220)
      );
    }
    button:hover:not(:disabled) {
      background-color: rgb(250, 250, 250);
    }
    button:active:not(:disabled) {
      transform: translateY(calc(var(--shadow-height) / 2));
    }
    button:disabled {
      cursor: not-allowed;
      background-color: lightgrey;
      color: white;
      filter: drop-shadow(0 var(--shadow-height) 0 rgb(167, 167, 167));
    }
  `;

  render() {
    return html`
      <button>
        <slot></slot>
      </button>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "gs6-button": Button;
  }
}
