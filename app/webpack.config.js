require("dotenv").config();

const webpack = require("webpack");
const path = require("path");
const config = require("sapper/config/webpack.js");
const pkg = require("./package.json");
const sveltePreprocess = require("svelte-preprocess");

const mode = process.env.NODE_ENV;
const dev = mode === "development";

const alias = { svelte: path.resolve("node_modules", "svelte") };
const extensions = [".ts", ".mjs", ".js", ".json", ".svelte", ".html"];

const emptyModulePath = path.resolve(__dirname, "src", "empty.js");

module.exports = {
  client: {
    entry: config.client.entry(),
    output: config.client.output(),
    resolve: {
      alias,
      extensions,
      mainFields: ["svelte", "module", "browser", "main"],
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: { loader: "ts-loader" },
        },
        { test: /\.(png|svg|jpg|css)$/i, use: "url-loader" },
        {
          test: /\.(svelte|html)$/,
          use: {
            loader: "svelte-loader",
            options: {
              dev,
              hydratable: true,
              hotReload: false, // pending https://github.com/sveltejs/svelte/issues/2377
              preprocess: sveltePreprocess(),
            },
          },
        },
      ],
    },
    mode,
    plugins: [
      // pending https://github.com/sveltejs/svelte/issues/2377
      // dev && new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
        API_HOST: JSON.stringify(process.env.API_HOST),
        API_TLS: process.env.API_TLS,
      }),
    ].filter(Boolean),
    devtool: dev ? "inline-source-map" : "source-map",
  },

  server: {
    entry: config.server.entry(),
    output: config.server.output(),
    target: "node",
    resolve: {
      alias: {
        ...alias,
        "@gamesite6/love-letter": emptyModulePath,
        "@gamesite6/fox-in-the-forest": emptyModulePath,
        "@gamesite6/no-thanks": emptyModulePath,
        "@gamesite6/shobu": emptyModulePath,
        "@gamesite6/yinsh": emptyModulePath,
        "@gamesite6/lost-cities": emptyModulePath,
        "@gamesite6/the-resistance": emptyModulePath,
        "@gamesite6/6-nimmt": emptyModulePath,
        "@gamesite6/sushi-go": emptyModulePath,
        "@gamesite6/hanabi": emptyModulePath,
        "@gamesite6/cant-stop": emptyModulePath,
      },
      extensions,
      mainFields: ["svelte", "module", "main", "browser"],
    },
    externals: Object.keys(pkg.dependencies).concat("encoding"),
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: { loader: "ts-loader" },
        },
        { test: /\.(png|svg|jpg|css)$/i, use: "url-loader" },
        {
          test: /\.(svelte|html)$/,
          use: {
            loader: "svelte-loader",
            options: {
              css: false,
              generate: "ssr",
              dev,
              preprocess: sveltePreprocess(),
            },
          },
        },
      ],
    },
    mode: process.env.NODE_ENV,
    performance: {
      hints: false, // it doesn't matter if server.js is large
    },
  },

  serviceworker: {
    entry: config.serviceworker.entry(),
    output: config.serviceworker.output(),
    mode: process.env.NODE_ENV,
  },
};
