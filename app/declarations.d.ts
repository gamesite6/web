type GameId = number;
type PlayerId = number;
type UserId = number;

type GameWithSettingsDto = {
  data: GameDto;
  settings: JSON;
};

type GameDto = {
  id: GameId;
  game_type: number;
  host: UserId;
  status: string;
  players: UserId[];
};

declare var API_TLS: boolean;
declare var API_HOST: string;

namespace process {
  var browser: boolean;
}

namespace grecaptcha {
  namespace enterprise {
    function execute(string, { action: string }): Promise<string>;
  }
}
