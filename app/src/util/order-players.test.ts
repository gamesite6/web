import { orderPlayers } from "./order-players";

test("order players with no player", () => {
  let allPlayers = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
  let orderedPlayers = orderPlayers(allPlayers);

  expect(orderedPlayers).toEqual([{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }]);
});

test("order with player", () => {
  let allPlayers = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
  let orderedPlayers = orderPlayers(allPlayers, 3);

  expect(orderedPlayers).toEqual([{ id: 3 }, { id: 4 }, { id: 1 }, { id: 2 }]);
});
