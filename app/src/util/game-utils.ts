export enum GameType {
  LoveLetter = 1,
  FoxInTheForest = 2,
  NoThanks = 3,
  Shobu = 4,
  Skull = 5,
  TheResistance = 6,
  Tichu = 7,
  Hanabi = 8,
  LiarsDice = 9,
  CockroachPoker = 10,
  Azul = 11,
  CantStop = 12,
  ForSale = 13,
  FantasyRealms = 14,
  Yinsh = 15,
  LostCities = 16,
  SechsNimmt = 17,
  SushiGo = 18,
}

export function gameTypeStringToNumber(gameType: string): GameType | undefined {
  switch (gameType) {
    case "love-letter":
      return GameType.LoveLetter;
    case "fox-in-the-forest":
      return GameType.FoxInTheForest;
    case "no-thanks":
      return GameType.NoThanks;
    case "shobu":
      return GameType.Shobu;
    case "skull":
      return GameType.Skull;
    case "the-resistance":
      return GameType.TheResistance;
    case "tichu":
      return GameType.Tichu;
    case "hanabi":
      return GameType.Hanabi;
    case "liars-dice":
      return GameType.LiarsDice;
    case "cockroach-poker":
      return GameType.CockroachPoker;
    case "azul":
      return GameType.Azul;
    case "cant-stop":
      return GameType.CantStop;
    case "for-sale":
      return GameType.ForSale;
    case "fantasy-realms":
      return GameType.FantasyRealms;
    case "yinsh":
      return GameType.Yinsh;
    case "lost-cities":
      return GameType.LostCities;
    case "6-nimmt":
      return GameType.SechsNimmt;
    case "sushi-go":
      return GameType.SushiGo;
  }
}

export function gameTypeNumberToString(gameType: GameType): string {
  switch (gameType) {
    case GameType.LoveLetter:
      return "love-letter";
    case GameType.FoxInTheForest:
      return "fox-in-the-forest";
    case GameType.NoThanks:
      return "no-thanks";
    case GameType.Shobu:
      return "shobu";
    case GameType.Skull:
      return "skull";
    case GameType.TheResistance:
      return "the-resistance";
    case GameType.Tichu:
      return "tichu";
    case GameType.Hanabi:
      return "hanabi";
    case GameType.LiarsDice:
      return "liars-dice";
    case GameType.CockroachPoker:
      return "cockroach-poker";
    case GameType.Azul:
      return "azul";
    case GameType.CantStop:
      return "cant-stop";
    case GameType.ForSale:
      return "for-sale";
    case GameType.FantasyRealms:
      return "fantasy-realms";
    case GameType.Yinsh:
      return "yinsh";
    case GameType.LostCities:
      return "lost-cities";
    case GameType.SechsNimmt:
      return "6-nimmt";
    case GameType.SushiGo:
      return "sushi-go";
  }
}

export function gameTypeName(gameType: GameType): string {
  switch (gameType) {
    case GameType.LoveLetter:
      return "Love Letter";
    case GameType.FoxInTheForest:
      return "The Fox in the Forest";
    case GameType.NoThanks:
      return "No Thanks!";
    case GameType.Shobu:
      return "Shōbu";
    case GameType.Skull:
      return "Skull";
    case GameType.TheResistance:
      return "The Resistance";
    case GameType.Tichu:
      return "Tichu";
    case GameType.Hanabi:
      return "Hanabi";
    case GameType.LiarsDice:
      return "Liar's Dice";
    case GameType.CockroachPoker:
      return "Cockroach Poker";
    case GameType.Azul:
      return "Azul";
    case GameType.CantStop:
      return "Can't Stop";
    case GameType.ForSale:
      return "For Sale";
    case GameType.FantasyRealms:
      return "Fantasy Realms";
    case GameType.Yinsh:
      return "Yinsh";
    case GameType.LostCities:
      return "Lost Cities";
    case GameType.SechsNimmt:
      return "6 Nimmt";
    case GameType.SushiGo:
      return "Sushi Go!";
  }
}

export function maxPlayers(gameType: GameType, settings: any): number {
  switch (gameType) {
    case GameType.LoveLetter:
      return Math.max(...settings.selectedPlayerCounts);
    case GameType.FoxInTheForest:
      return 2;
    case GameType.NoThanks:
      return Math.max(...settings.selectedPlayerCounts);
    case GameType.Shobu:
      return 2;
    case GameType.Skull:
      return Math.max(...settings);
    case GameType.TheResistance:
      return Math.max(...settings.selectedPlayerCounts);
    case GameType.Tichu:
      return 4;
    case GameType.Hanabi:
      return Math.max(...settings.selectedPlayerCounts);
    case GameType.LiarsDice:
      return Math.max(...settings["selected-player-counts"]);
    case GameType.CockroachPoker:
      return Math.max(...settings.selectedPlayerCounts);
    case GameType.Azul:
      return Math.max(...settings.selectedPlayerCounts);
    case GameType.CantStop:
      return Math.max(...settings.playerCounts);
    case GameType.ForSale:
      return Math.max(...settings.playerCounts);
    case GameType.FantasyRealms:
      return Math.max(...settings.playerCounts);
    case GameType.Yinsh:
      return 2;
    case GameType.LostCities:
      return 2;
    case GameType.SechsNimmt:
      return Math.max(...settings.playerCounts);
    case GameType.SushiGo:
      return Math.max(...settings.playerCounts);
  }
}

type Players = { [playerId: string]: UserId };

export function getPlayerIdFromUserId(
  players: Players,
  userId: UserId
): PlayerId | undefined {
  for (const pid in players) {
    if (players[pid] === userId) {
      return parseInt(pid);
    }
  }
}
