export function orderPlayers(allPlayers, playerId?: PlayerId) {
  let playerIndex = allPlayers.findIndex((player) => player.id === playerId);

  if (playerIndex === -1) {
    return allPlayers;
  } else {
    let player = allPlayers[playerIndex];

    let playersBeforePlayer = allPlayers.slice(0, playerIndex);
    let playersAfterPlayer = allPlayers.slice(playerIndex + 1);

    return [player, ...playersAfterPlayer, ...playersBeforePlayer];
  }
}

export function orderPlayerIds(
  allPlayerIds: PlayerId[],
  playerId?: PlayerId
): PlayerId[] {
  let idx = allPlayerIds.findIndex((pid) => pid === playerId);
  if (idx === -1) {
    return allPlayerIds;
  } else {
    let beforePlayer = allPlayerIds.slice(0, idx);
    let afterPlayer = allPlayerIds.slice(idx + 1);

    return [playerId, ...afterPlayer, ...beforePlayer];
  }
}
