import groupAdjacentBy from "./group-adjacent";

test("group adjacent by equality", () => {
  let array = [1, 1, 2, 2, 3, 3];
  let result = groupAdjacentBy({
    predicate(acc, prev, curr) {
      return prev === curr;
    },
    initGroup(a) {
      return [a];
    },
    combine(acc, a) {
      acc.push(a);
      return acc;
    },
  })(array);

  expect(result).toEqual([
    [1, 1],
    [2, 2],
    [3, 3],
  ]);
});

test("group adjacent by predicate", () => {
  let array = [1, 2, 3, 5, 6, 9, 8, 7];

  let result = groupAdjacentBy({
    predicate(acc, prev, a) {
      return Math.abs(prev - a) < 2;
    },
    initGroup(a) {
      return a;
    },
    combine(acc, a) {
      return acc + a;
    },
  })(array);

  expect(result).toEqual([6, 11, 24]);
});

test("group messages", () => {
  let messages = [
    { user: 1, time: 1, text: "a" },
    { user: 1, time: 1, text: "b" },
    { user: 2, time: 1, text: "c" },
    { user: 2, time: 2, text: "d" },
    { user: 2, time: 2, text: "e" },
  ];

  let result = groupAdjacentBy({
    predicate(acc, prev, a) {
      return prev.user == a.user && prev.time == a.time;
    },
    initGroup(msg) {
      return {
        ...msg,
        text: [msg.text],
      };
    },
    combine(acc, msg) {
      acc.text.push(msg.text);
      return acc;
    },
  })(messages);

  expect(result).toEqual([
    { user: 1, time: 1, text: ["a", "b"] },
    { user: 2, time: 1, text: ["c"] },
    { user: 2, time: 2, text: ["d", "e"] },
  ]);
});
