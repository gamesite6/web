export function loadRecaptcha() {
  if (process.browser && window.location.hostname === "www.gamesite6.com") {
    const script = document.createElement("script");

    script.src =
      "https://www.google.com/recaptcha/enterprise.js?render=6LeCGmkaAAAAAKeuYOlQdRms0iJQkYv8rrZ7BtAJ";
    script.id = "recaptcha-script";
    script.async = true;

    document.body.append(script);
  }
}

export async function getRecaptchaToken(action: string) {
  if (process.browser && window.location.hostname === "www.gamesite6.com") {
    return await grecaptcha.enterprise.execute(
      "6LeCGmkaAAAAAKeuYOlQdRms0iJQkYv8rrZ7BtAJ",
      { action: "register" }
    );
  } else {
    return "";
  }
}

export function removeRecaptcha() {
  if (process.browser) {
    const script = document.getElementById("recaptcha-script");
    if (script) {
      script.remove();
    }

    const recaptchaElems = document.getElementsByClassName("grecaptcha-badge");
    if (recaptchaElems.length) {
      recaptchaElems[0].parentElement.remove();
    }
  }
}
