import "@gamesite6/elements";
import * as sapper from "@sapper/app";
import { initClientStores } from "./stores";

import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

if (window.location.hostname === "www.gamesite6.com") {
  Sentry.init({
    dsn: "https://df69e30bd8d74cef9b4db15a21aedade@o524012.ingest.sentry.io/5636410",
    integrations: [new Integrations.BrowserTracing()],
    tracesSampleRate: 1.0,
  });
}

initClientStores().then(() => {
  sapper.start({
    target: document.querySelector("#sapper"),
  });
});
