import sirv from "sirv";
import polka from "polka";
import compression from "compression";
import * as sapper from "@sapper/server";
import { initServerStores } from "./stores";

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

initServerStores();

polka() // You can also use Express
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware()
  )
  .listen(PORT, "0.0.0.0", (err) => {
    if (err) console.log("error", err);
  });
