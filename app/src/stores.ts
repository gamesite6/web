import { writable } from "svelte/store";
import * as shared from "@gamesite6/shared";
import { encode, decode } from "@msgpack/msgpack";
import { gameTypeStringToNumber, GameType } from "./util/game-utils";
shared.users.init(getUsersStore, getUserData);

let websocket;

function sendJson(msgJson) {
  websocket.send(encode(msgJson));
}

// stores
let user;
let users;
const chatrooms = {};
const games = {};
const lobbies = {};

async function websocketListener(event) {
  const msgBytes = await event.data.arrayBuffer();
  const msg = decode(msgBytes);

  if (msg[0] !== undefined) {
    user.set({ id: msg[0], games: null });
  } else if (msg[4] !== undefined) {
    const lobby = getLobbyStore(msg[4].game_type);
    lobby.set(msg[4]);
  } else if (msg[1] !== undefined) {
    const msgGames = msg[1];

    user.update((state) => {
      const newGames = {};
      for (const game of msgGames.games) {
        newGames[game.data.id] = game;
      }

      state.games = newGames;
      return state;
    });
  } else if (msg[5] !== undefined) {
    const lobby = getLobbyStore(msg[5].game.data.game_type);
    lobby.update((state) => {
      state.games.unshift(msg[5].game);
      return state;
    });
    const game = msg[5].game;
    const gameId = game.data.id;
    const gameHost = game.data.host;
    user.update((state) => {
      if (
        state &&
        (state.id === gameHost ||
          Object.values(game.data.players).includes(state.id))
      ) {
        state.games = state.games || {};
        state.games[game.data.id] = game;
      }
      return state;
    });
  } else if (msg[2] !== undefined) {
    const room = msg[2].room;
    const store = getChatroomStore(room);

    const chatMessage = {
      userId: msg[2].usr,
      text: msg[2].msg,
      time: Date.parse(msg[2].time),
    };
    store.update((messages) => {
      messages.push(chatMessage);
      return messages;
    });
  } else if (msg[3] !== undefined) {
    const room = msg[3].room;
    const store = getChatroomStore(room);
    const messages = msg[3].msgs.map((msg) => {
      return {
        userId: msg.usr,
        text: msg.msg,
        time: Date.parse(msg.time),
      };
    });
    store.set(messages);
  } else if (msg[6] !== undefined) {
    const updated = msg[6];
    const gameId = updated.game.id;
    const store = getGameStore(gameId, updated.game.game_type);
    store.update((prev) => {
      let game = prev || {};
      game.data = updated.game;
      return game;
    });

    if (updated.game.status === "NOT_STARTED") {
      const lobby = getLobbyStore(updated.game.game_type);
      lobby.update((state) => {
        let existingGame = state.games.find((g) => g.data.id === gameId);
        if (existingGame) {
          existingGame.data = updated.game;
        }
        return state;
      });
    }

    user.update((state) => {
      if (state && state.id) {
        const userId = state.id;
        if (
          userId === updated.game.host ||
          Object.values(updated.game.players).includes(userId)
        ) {
          state.games = state.games || {};
          state.games[gameId] = state.games[gameId] || {};
          state.games[gameId].data = updated.game;
        } else if (state.games) {
          delete state.games[gameId];
        }
      }
      return state;
    });
  } else if (msg[8] !== undefined) {
    const msgGame = msg[8];
    const gameId = msgGame.id;
    const store = getGameStore(gameId);
    store.update((prev) => {
      const game = prev || {};
      if (msgGame.data) {
        game.data = msgGame.data;
      }
      if (msgGame.settings) {
        game.settings = msgGame.settings;
      }
      if (msgGame.state) {
        game.state = msgGame.state;
      }
      return game;
    });

    if (msgGame.data && msgGame.data.status === "NOT_STARTED") {
      const lobby = getLobbyStore(msgGame.data.game_type);
      lobby.update((state) => {
        const existingGame = state.games.find(
          (g) => g.data.id == msgGame.data.id
        );
        if (existingGame) {
          existingGame.data = msgGame.data;
          if (msgGame.settings) {
            existingGame.settings = msgGame.settings;
          }
        }
        return state;
      });
    }

    user.update((state) => {
      if (state && state.id && msgGame.data) {
        const userId = state.id;
        if (
          userId === msgGame.data.host ||
          Object.values(msgGame.data.players).includes(userId)
        ) {
          state.games = state.games || {};
          const existingGame = state.games[msgGame.data.id];

          state.games[msgGame.data.id] = msgGame;

          if (!msgGame.settings && existingGame) {
            state.games[msgGame.data.id].settings = existingGame.settings;
          }
          if (!msgGame.state && existingGame) {
            state.games[msgGame.data.id].state = existingGame.state;
          }
        } else if (state.games) {
          delete state.games[msgGame.data.id];
        }
      }
      return state;
    });
  } else if (msg[7] !== null) {
    const msgStarted = msg[7];
    const gameId = msgStarted.id;

    getLobbyStore(msgStarted.game_type).update((state) => {
      let gameIndex = state.games.findIndex((g) => g.data.id === gameId);
      if (gameIndex !== -1) {
        state.games.splice(gameIndex, 1);
      }
      return state;
    });
  }
}

export function initServerStores() {
  websocket = {
    send() {},
    addEventListener() {},
    removeEventListener() {},
  };
  user = writable(null);
  users = writable({});
}

export async function initClientStores() {
  await initWebSocket();

  user = writable(null);
  users = writable({});
}

let reconnectAttempts = 0;

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function initWebSocket() {
  return new Promise((resolve) => {
    websocket = new WebSocket(`${API_TLS ? "wss" : "ws"}://${API_HOST}/api/ws`);

    function handleOpen() {
      sendJson({ 0: null });
      reconnectAttempts = 0;
      websocket.removeEventListener("open", handleOpen);

      resolve(undefined);
    }
    websocket.addEventListener("open", handleOpen);
    websocket.addEventListener("message", websocketListener);
    websocket.addEventListener("close", async () => {
      const waitTime =
        reconnectAttempts > 0 ? 500 * Math.pow(2, reconnectAttempts) : 0;

      reconnectAttempts += 1;

      await sleep(waitTime);
      await initWebSocket();

      resolve(undefined);
    });
  });
}

export function joinGame(
  gameId: GameId,
  playerId: PlayerId,
  gameType: string | GameType
) {
  if (typeof gameType === "string") {
    gameType = gameTypeStringToNumber(gameType);
  }
  sendJson({ 8: [gameId, playerId, gameType] });
}

export function leaveGame(gameId: GameId, gameType: string | GameType) {
  if (typeof gameType === "string") {
    gameType = gameTypeStringToNumber(gameType);
  }
  sendJson({ 9: [gameId, gameType] });
}

export function startGame(gameId: GameId, gameType: string | GameType) {
  if (typeof gameType === "string") {
    gameType = gameTypeStringToNumber(gameType);
  }
  sendJson({ 10: [gameId, gameType] });
}

export function performAction(
  gameId: GameId,
  gameType: string | GameType,
  action
) {
  if (typeof gameType === "string") {
    gameType = gameTypeStringToNumber(gameType);
  }

  sendJson({ 11: [gameId, gameType, action] });
}

export function getLobbyStore(gameType: GameType) {
  if (!lobbies[gameType]) {
    const store = writable({ games: [] }, function start(set) {
      set({ games: [] });
      sendJson({ 4: gameType });

      return function stop() {
        sendJson({ 5: gameType });
      };
    });
    lobbies[gameType] = store;
  }

  return lobbies[gameType];
}

export function getChatroomStore(room) {
  if (!chatrooms[room]) {
    const store = writable([], function start(set) {
      set([]);
      sendJson({ 1: room });

      return function stop() {
        sendJson({ 2: room });
        delete chatrooms[room];
      };
    });

    chatrooms[room] = store;
  }
  return chatrooms[room];
}

export function getGameStore(
  gameId: GameId,
  gameType: GameType | string | undefined = undefined
) {
  if (games[gameId]) {
    return games[gameId];
  }

  if (typeof gameType === "string") {
    gameType = gameTypeStringToNumber(gameType);
  }

  if (!gameId || !gameType) {
    return writable(null);
  }

  if (!games[gameId]) {
    const store = writable(null, function start(set) {
      set(null);
      sendJson({ 6: [gameId, gameType] });

      return function stop() {
        sendJson({ 7: gameId });
        delete games[gameId];
      };
    });
    games[gameId] = store;
  }

  return games[gameId];
}

export async function createGame(gameType: GameType, settings) {
  const res = await fetch(
    `${API_TLS ? "https" : "http"}://${API_HOST}/api/games`,
    {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        game_type: gameType,
        settings,
      }),
    }
  );
  if (res.ok) {
    const json = await res.json();
    return json;
  }
}

export function getUserStore() {
  return user;
}

export function sendMessage({ room, msg }) {
  sendJson({ 3: [room, msg] });
}

export function getUsersStore() {
  return users;
}

const userDataRequests = {};
export async function getUserData(userId) {
  if (userDataRequests[userId]) {
    return await userDataRequests[userId];
  } else {
    const req = fetch(
      `${API_TLS ? "https" : "http"}://${API_HOST}/api/users/${userId}`
    ).then((res) => res.json());
    userDataRequests[userId] = req;
    const userData = await req;

    users.update((state) => {
      state[userId] = userData;
      return state;
    });

    return userData;
  }
}
