require("dotenv").config();

const path = require("path");
const sveltePreprocess = require("svelte-preprocess");

module.exports = {
  entry: path.resolve(__dirname, "src", "index.ts"),
  mode: "development",
  devtool: "eval-source-map",
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "main.js",
    library: "DevTool",
    libraryTarget: "umd",
  },
  resolve: {
    extensions: [".ts", ".mjs", ".js", ".svelte"],
    mainFields: ["svelte", "browser", "module", "main"],
    alias: {
      "@gamesite6/game": process.env.GAME_CLIENT_PATH,
    },
  },
  module: {
    rules: [
      { test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/ },
      { test: /\.(png|svg)$/i, use: "url-loader" },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(html|svelte)$/,
        exclude: /node_modules/,
        use: {
          loader: "svelte-loader",
          options: {
            preprocess: sveltePreprocess(),
          },
        },
      },
    ],
  },
};
