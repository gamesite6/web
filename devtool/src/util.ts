export function range(a: number, b?: number): number[] {
  const start = b === undefined ? 0 : a;
  const end = b === undefined ? a : b;

  const size = end - start;
  const result = Array(size);
  for (let i = 0; i < size; i++) {
    result[i] = start + i;
  }
  return result;
}

export const INT32_MAX = 2_147_483_647;

export function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}
