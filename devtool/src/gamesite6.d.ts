declare module "@gamesite6/game" {
  export const Game: CustomElementConstructor;
  export const Reference: CustomElementConstructor;
}

declare module "@gamesite6/game/src/main" {
  export const Game: CustomElementConstructor;
  export const Reference: CustomElementConstructor;
}

declare module "@gamesite6/shared" {
  export const users: any;
}

type PlayerId = number;

type Model = {
  playerCount: number;
  userId: PlayerId | undefined;
  seedInput: string;
  seed: number | undefined;
  settings: string;
  settingsInput: string;
  state: string;
  stateInput: string;
};

declare module "*.svg" {
  const content: string;
  export default content;
}
declare module "*.png" {
  const content: string;
  export default content;
}

declare module "*.jpg" {
  const content: string;
  export default content;
}

declare module "*.css" {
  const content: Record<string, string>;
  export default content;
}
