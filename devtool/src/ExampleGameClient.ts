type PlayerId = number;
type State = {};
type Settings = {};
type Action = {};

export class Game extends HTMLElement {
  #state?: State;
  #settings?: Settings;

  set state(state: State) {
    this.#state = state;
    this.rerender();
  }
  set settings(settings: Settings) {
    this.#settings = settings;
    this.rerender();
  }

  get userId(): PlayerId | null {
    let userId: number = parseInt(this.getAttribute("user-id") ?? "");
    if (Number.isInteger(userId)) {
      return userId;
    } else {
      return null;
    }
  }

  private rerender() {
    if (this.#state !== undefined && this.#settings !== undefined) {
      this.innerText = "component";
    }
  }

  connectedCallback() {
    this.rerender();
  }
  static get observedAttributes() {
    return ["user-id"];
  }
  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (oldValue !== newValue) {
      this.rerender();
    }
  }
}

export class Reference extends HTMLElement {
  #settings?: Settings;

  constructor() {
    super();
  }

  set settings(settings: Settings) {
    this.#settings = settings;
    this.rerender();
  }

  private rerender() {
    this.innerText = "reference";
  }
  connectedCallback() {
    this.rerender();
  }

  static get observedAttributes() {
    return ["settings"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (oldValue !== newValue) {
      this.rerender();
    }
  }
}
