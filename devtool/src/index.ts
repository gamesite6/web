import { users } from "@gamesite6/shared";

import DevTool from "./DevTool.svelte";
import { Game, Reference } from "@gamesite6/game/src/main";
import { writable } from "svelte/store";

window.customElements.define("game-component", Game);
window.customElements.define("reference-component", Reference);

let usersStore = writable({
  "1": { id: 1, name: "ashley_1" },
  "2": { id: 2, name: "ben_2" },
  "3": { id: 3, name: "charlotte_3" },
  "4": { id: 4, name: "daniel_4" },
  "5": { id: 5, name: "emma_5" },
  "6": { id: 6, name: "fred_6" },
  "7": { id: 7, name: "george_7" },
  "8": { id: 8, name: "hannah_8" },
  "9": { id: 9, name: "isaac_9" },
  "10": { id: 10, name: "james_10" },
});

users.init(
  () => usersStore,
  (userId: number) => {
    return Promise.resolve({
      id: userId,
      name: "ERROR: Unknown user id",
    });
  }
);

let model: Model | undefined = undefined;
try {
  model = JSON.parse(localStorage.getItem("model") ?? "");
} catch (e) {
  console.warn(e);
}

const app = new DevTool({
  target: document.body,
  props: {
    model,
  },
});

app.$on("change", (ev) => {
  const model = ev.detail;
  localStorage.setItem("model", JSON.stringify(model));
});
