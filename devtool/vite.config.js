import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

import dotenv from "dotenv";
dotenv.config();

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte()],
  resolve: {
    alias: {
      "@gamesite6/game": process.env.GAME_CLIENT_PATH,
    },
  },
  optimizeDeps: {
    include: ["@gamesite6/shared"],
  },
  server: {
    proxy: {
      "/initial-state": {
        target: process.env.GAME_SERVER_BASE_URL,
      },
      "/perform-action": {
        target: process.env.GAME_SERVER_BASE_URL,
      },
    },
  },
});
