import mouth1 from "./mouth1_64.png";
import mouth3 from "./mouth3_64.png";
import mouth5 from "./mouth5_64.png";
import mouth6 from "./mouth6_64.png";
import mouth7 from "./mouth7_64.png";
import mouth9 from "./mouth9_64.png";
import mouth10 from "./mouth10_64.png";
import mouth11 from "./mouth11_64.png";

export default [
  mouth1,
  mouth3,
  mouth5,
  mouth6,
  mouth7,
  mouth9,
  mouth10,
  mouth11,
];
