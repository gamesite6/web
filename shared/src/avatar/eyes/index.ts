import eyes1 from "./eyes1_64.png";
import eyes2 from "./eyes2_64.png";
import eyes3 from "./eyes3_64.png";
import eyes4 from "./eyes4_64.png";
import eyes5 from "./eyes5_64.png";
import eyes6 from "./eyes6_64.png";
import eyes7 from "./eyes7_64.png";
import eyes9 from "./eyes9_64.png";
import eyes10 from "./eyes10_64.png";

export default [eyes1, eyes2, eyes3, eyes4, eyes5, eyes6, eyes7, eyes9, eyes10];
