const sveltePreprocess = require("svelte-preprocess");

const path = require("path");

const mode = process.env.NODE_ENV || "development";
const prod = mode === "production";

module.exports = {
  entry: {
    "gs6-shared": ["./src/main.js"],
    "gs6-shared-browser": ["./src/browser.js"],
  },
  resolve: {
    alias: {
      svelte: path.resolve("node_modules", "svelte"),
    },
    extensions: [".ts", ".js", ".svelte"],
    mainFields: ["svelte", "browser", "module", "main"],
  },
  output: {
    libraryTarget: "commonjs2",
    path: __dirname + "/public",
    filename: "[name].js",
    chunkFilename: "[name].[id].js",
  },
  module: {
    rules: [
      { test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/ },
      { test: /\.(png|svg)$/i, use: "url-loader" },
      {
        test: /\.svelte$/,
        use: {
          loader: "svelte-loader",
          options: {
            customElement: true,

            emitCss: true,
            preprocess: sveltePreprocess({
              postcss: {
                plugins: [require("postcss-preset-env")()],
              },
            }),
          },
        },
      },
    ],
  },
  mode,
  devtool: prod ? false : "source-map",
};
